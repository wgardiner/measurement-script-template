/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * !!!IMPORTANT!!! for testing
 * Add crentials to mock/credentials.json (don't commit to repo)
 * format in mock/credentials.demo.json
 */
// https://test.farmos.net/admin/structure/taxonomy/farm_log_categories

import moment from 'moment';

import {
  app,
  formparser,
  serial
} from '@oursci/scripts';
// import errorCheck from './errorcheck';

// import surveyHelper from '../.oursci-surveys/survey';

export default serial; // expose this to android for calling onDataAvailable

// if in dev environment, take this survey result as example
const uuidSnippet = '28c68e37';
const form = 'post-planting-anon-test';

if (app.initSubmittedMock && uuidSnippet && form) {
  app.initSubmittedMock(form, uuidSnippet);
  console.log(`fetched survey result for uuid: ${uuidSnippet}`);
}

const result = {};
result.log = [];
result.error = [];

// export const survey = surveyHelper();
export function err(msg) {
  result.error.push(msg);
}

function appendResult(title, content) {
  result.log.push({
    title,
    content,
  });
}

function errorExit(error) {
  app.error();
  throw Error(error);
}

(async () => {
  try {
    // Generate some dummy results
    const indexArray = Array.from(Array(10).keys())
      .forEach(i => appendResult(`Title ${i}`, `Content ${i}`));

    app.result(result);
  } catch (error) {
    console.error(error);
    result.error = [error.message];
    app.result(result);
  }
})();