# Measurement Script Template

This repository contains the boilerplate for creating a new measurement script

It is intended to be used with [`degit`](https://github.com/Rich-Harris/degit) to scaffold new scripts.

```bash
# Install degit
npm install -g degit
# use degit to scaffold measurement script boilerplate
degit https://gitlab.com/wgardiner/measurement-script-template my-new-script
cd my-new-script
# install dependencies
npm install
# or
yarn install
```

Then go mess with `sensor.js` and `processor.js` and run with:

```
# Run sensor script initially to generate /data/results.json
npm run sensor
# Run processor dev server
npm run run-processor-dev
```

## Using with OurSci VSCode Extension

The OurSci VSCode extension lets you download sample data from existing OurSci survey results and reference them in the measurement script dev environment

- Install OurSci VSCode extension
- `Ctrl` + `Shift` + `P` and type "oursci", hit `Enter`
- Select the survey results you'd like to download, hit `Enter`
- The selected survey results will be downloaded to the `.oursci-surveys` folder as `survey.csv` along with helper script (`survey.js`) for accessing these results via a measurement script
- In your measurement script, you can now reference the existing results as follows

```javascript
import surveyHelper from "../.oursci-surveys/survey";
export const survey = surveyHelper();
```

#TODO: finish this... how does this help you autocomplete

## Using Survey Metadata with `extractOptions`

- In ODKBuild select `File` > `Save Form to File`
- Rename/move form file to `mock/odkbuild.json`

```bash
cd mock
node extractOptions.js > meta.json
```

#TODO: finish this... show example of consuming meta.json
